/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <QtGlobal>
#include <QtDebug>
#include <QString>
#include <QCoreApplication>
#include <QHostAddress>
#include <QVariantMap>
#include <QObject>
#include <QApplication>

#undef DELETE
#include <qhttpengine/filesystemhandler.h>
#include <qhttpengine/handler.h>
#include <qhttpengine/server.h>
#include <qhttpengine/qobjecthandler.h>
#include <qhttpengine/socket.h>

#include "visplay/Window.h"
#include "visplay/CommandLineParser.h"


int main(int argc, char * argv[])
{
    QApplication a(argc, argv);

    visplay::CommandLineParser parser(a);

    QHttpEngine::Server server;
    visplay::Window win(argc, argv);

    QHttpEngine::QObjectHandler handler;

    // endpoints for server ping
    handler.registerMethod("ping", &win, &visplay::Window::ping);
    
    // endpoints for controlling general visplay state
    handler.registerMethod("swap_buffers", &win, &visplay::Window::swap_buffers);

    // endpoints for communicating with the front buffer's mpv
    handler.registerMethod("mpv/play", &win, &visplay::Window::open_media);
    handler.registerMethod("mpv/idle_status", &win, &visplay::Window::get_idle_status);
    handler.registerMethod("mpv/get_property", &win, &visplay::Window::get_property);

    // endpoints for controling back buffer
    handler.registerMethod("backbuffer/change_layout", &win, &visplay::Window::backbuffer_change_layout);

    handler.registerMethod("long_poll_test", &win, &visplay::Window::long_poll_test);

    server.setHandler(&handler);
    if (!server.listen(parser.getAddress(), parser.getPort())) {
        qCritical("unable to bind to a local port");
        a.exit(1);
    }

    return a.exec();
}
