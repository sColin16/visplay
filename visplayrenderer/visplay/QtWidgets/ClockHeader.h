/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CLOCKHEADER_H
#define CLOCKHEADER_H

#include <QWidget>
#include <QString>
#include <QLabel>
#include <QPointer>
#include <QHBoxLayout>

#include "visplay/QtWidgets/DigitalClock.h"

class ClockHeader : public QWidget
{
    Q_OBJECT

    public:
        ClockHeader(QWidget *parent = NULL, 
                    QString headerText = "",
                    QString fontString = "Arial",
                    int fontS = 24,
                    QString headerStyleSheet = "QLabel { background-color : #C46342 ; color : white; }", 
                    QString clockStyleSheet = "QLabel { background-color : #C46342 ; color : white; }",
                    int clockWidth = 200);
        ~ClockHeader() {}

        void setHeaderText(QString newText);
        QString getHeaderText();

        QTime getTime();
    private:
        int fontSize;
        QString fontType;

        QPointer<QLabel> header;
        QPointer<DigitalClock> dclock;
};

#endif