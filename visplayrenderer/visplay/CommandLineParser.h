/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COMMAND_LINE
#define COMMAND_LINE

#include <QString>
#include <QCommandLineParser>
#include <QHostAddress>
#include <QApplication>

namespace visplay
{

class CommandLineParser
{
    public:
        CommandLineParser(QApplication &a);

        int getPort();
        QHostAddress getAddress();

    private:
        QCommandLineParser parser;

        QString application_description;
        QHostAddress default_address;
        QString default_port;
        QHostAddress current_address;
        int current_port;

        bool _addressSanityChecking(QHostAddress address);
        bool _portSanityChecking(int port);

};

}
#endif