#include "BaseLayout.h"

namespace visplay::layouts {

// Compares json object keys to keys asked for by layout
bool BaseLayout::validate_json_keys(QJsonObject object) {
    QStringList keys = object.keys();

    QStringListIterator iterator(json_args);
    while (iterator.hasNext()) {
        if (!keys.contains(iterator.next())) {
            return false;
        }
    }
    return true;
}

void BaseLayout::validate(QJsonObject object) {
    valid = valid && validate_json_keys(object);
}

}
