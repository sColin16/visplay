/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef BASELAYOUT_H
#define BASELAYOUT_H

#include <QObject>
#include <QLayout>
#include <QEventLoop>
#include <QJsonObject>
#include <QStringList>
#include <QDebug>

namespace visplay::layouts {

class BaseLayout : public QObject
{

    Q_OBJECT

    public:
        BaseLayout() {};
        ~BaseLayout() {};

        virtual void display() = 0;
        virtual void connect_event_loop(QEventLoop* event_loop) = 0;
        
        QStringList json_args;
        bool valid = true;

        QLayout* layout;
        
    protected:
        bool validate_json_keys(QJsonObject object);
        void validate(QJsonObject);
};

} // end namespace visplay::layouts
#endif // BASELAYOUT_H
