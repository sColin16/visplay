/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "visplay/layouts/BBWLobbyLayoutText.h"

#include "visplay/QtWidgets/DigitalClock.h"

#include <QtWidgets>

namespace visplay::layouts 
{

BBWLayout::BBWLayout(QJsonObject& obj) 
{
    json_args.append(argHeader);
    json_args.append(argBody);

    validate(obj);

    fontType = "Arial";
    fontSize = 24;
    QFont font(fontType, fontSize);

    layout = new QVBoxLayout();
    
    QPointer<ClockHeader> headerClock = new ClockHeader;
    headerClock->setFixedHeight(fontSize + 10 * (fontSize / 12));

    QPointer<QLabel> body = new QLabel();
    body->setFont(font);

    int stuff = 4;
    int *a = &stuff
    body->setStyleSheet("QLabel { background-color : #3D4D7E ; color : white; }");
    body->setAlignment(Qt::AlignTop);

    headerText = obj[argHeader].toString();
    bodyText = obj[argBody].toString();

    headerClock->setHeaderText(headerText);
    body->setText(bodyText);    
    body->setWordWrap(true);

    layout->addWidget(headerClock);
    layout->addWidget(body);
     
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    qDebug() << "Creating BBWLobbyLayout";
}

BBWLayout::~BBWLayout()
{

}

void BBWLayout::display() 
{

}

void BBWLayout::connect_event_loop(QEventLoop* event_loop)
{
    
}

}
