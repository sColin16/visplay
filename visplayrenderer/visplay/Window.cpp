/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

// Create struct of type with certain ideas

#include "visplay/Window.h"
#include "visplay/layouts/DefaultLayout.h"
#include "visplay/layouts/FullscreenMPV.h"
#include "visplay/layouts/WebSplit.h"
#include "visplay/layouts/BBWLobbyLayoutText.h"
#include "visplay/layouts/BBWLobbyLayoutVideo.h"
#include "visplay/layouts/Identifiers.h"
#include "mpvwidget.h"

#include <QString>
#include <QDebug>
#include <QDialog>
#include <QLayout>
#include <QTimer>
#include <QEventLoop>
#include <QVBoxLayout>
#include <QPushButton>
#include <QFileDialog>
#include <QtGui/QOpenGLContext>
#include <QUrl>
#include <QThread>
#include <QSysInfo>

#include <QJsonDocument>
#include <QJsonObject>

#include <mpv/client.h>

namespace visplay
{

Window::Window(int argc, char *argv[])
{

    playback_idle                  = true;

    win                            = new QWidget();

    layouts::DefaultLayout* default_layout = new layouts::DefaultLayout("Welcome to Visplay!");
    win->setLayout(default_layout->layout);
    win->show();

    back_buffer = nullptr;
    /*
    QObject::connect(current_layout->mpv_widget, SIGNAL(playback_idle()),
                     this,       SLOT(mark_idle()));
    */
}

Window::~Window() {

}

void Window::open_media(QHttpEngine::Socket *httpSocket)
{
    QJsonDocument doc;
    QJsonObject obj;
    QString path;

    if (httpSocket->method() != QHttpEngine::Socket::POST) {
        send_text_reply(httpSocket, QHttpEngine::Socket::MethodNotAllowed, "/open_media only supports POST.\n");
        return;
    }

    httpSocket->readJson(doc);
    obj = doc.object();
    path = obj["path"].toString();
    qDebug() << "Path: " << path;

    mpv_widget->command(QStringList() << "loadfile" << path);

    playback_idle = false;

    send_text_reply(httpSocket, QHttpEngine::Socket::OK, "Playing requested media. \n");
}

void Window::get_idle_status(QHttpEngine::Socket *httpSocket)
{
    QString idle_string = QString::number(playback_idle) + "\n";
    QByteArray idle_byte_array = idle_string.toUtf8();

    if (httpSocket->method() != QHttpEngine::Socket::GET) {
        send_text_reply(httpSocket, QHttpEngine::Socket::MethodNotAllowed, "/get_idle_status only supports GET.\n");
        return;
    }

    send_text_reply(httpSocket, QHttpEngine::Socket::OK, idle_byte_array);
}

void Window::ping(QHttpEngine::Socket *httpSocket)
{
    QJsonObject obj;
    QJsonObject vals;

    vals.insert("status", status); // Report status, not currently implemented
    vals.insert("arch", QSysInfo::buildCpuArchitecture());
    vals.insert("distro", QSysInfo::productType());
    vals.insert("os", QSysInfo::kernelType());

    obj.insert("visplay-response", vals);

    QJsonDocument doc(obj);

    send_text_reply(httpSocket, QHttpEngine::Socket::OK, doc.toJson());
}

void Window::mark_idle()
{
    playback_idle = true;
}

void Window::get_property(QHttpEngine::Socket *httpSocket)
{
    QJsonDocument doc;
    QJsonObject   obj;
    QString       property_name;
    QVariant      property_value;
    QString       outgoing_string;


    if (httpSocket->method() != QHttpEngine::Socket::POST) {
        send_text_reply(httpSocket, QHttpEngine::Socket::MethodNotAllowed, "/get_property only supports POST.\n");
        return;
    }


    httpSocket->readJson(doc);
    obj = doc.object();
    property_name = obj["property"].toString();

    qDebug() << "property_name: " << property_name;

    property_value = mpv_widget->getProperty(property_name);

    qDebug() << "property_value: " << property_value;

    if (property_value.toString() == "") {
        outgoing_string = "ERROR";
    } else {
        outgoing_string = property_value.toString();
    }

    send_text_reply(httpSocket, QHttpEngine::Socket::OK, outgoing_string.toUtf8() + "\n");
}

void Window::swap_buffers(QHttpEngine::Socket *httpSocket)
{
    if (back_buffer == nullptr) {
	    send_text_reply(httpSocket, QHttpEngine::Socket::Conflict, "Could not swap buffers: back buffer uninitialized\n");
	    return;
    }

    // Create dummy QWidget that goes out of scope, taking the old layout with it.
    QWidget().setLayout(win->layout());
    front_buffer = back_buffer;
    // NULL out back buffer to prevent segfault on multiple swap_buffer calls
    back_buffer = nullptr; 
    win->setLayout(front_buffer->layout);
    win->show();
    front_buffer->display();

    send_text_reply(httpSocket, QHttpEngine::Socket::OK, "Buffers swapped.\n");

}

void Window::backbuffer_change_layout(QHttpEngine::Socket *httpSocket)
{
    QJsonDocument doc;
    QJsonObject obj;
    int new_layout_number;

    if (httpSocket->method() != QHttpEngine::Socket::POST) {
        send_text_reply(httpSocket, QHttpEngine::Socket::MethodNotAllowed, "/backbuffer/change_layout only supports POST.\n");
        return;
    }

    httpSocket->readJson(doc);
    obj = doc.object();
    new_layout_number = obj["layout"].toInt();
    back_buffer_layout_number = new_layout_number;

    switch (layouts::Identifiers(new_layout_number))
    {
        case layouts::Identifiers::DefaultLayout:
            back_buffer = new visplay::layouts::DefaultLayout(obj);
            break;
        case layouts::Identifiers::FullscreenMPV:
            back_buffer = new visplay::layouts::FullscreenMPV(obj);
            break;
        case layouts::Identifiers::WebSplit:
            back_buffer = new visplay::layouts::WebSplit(obj);
            break;
        case layouts::Identifiers::BBWLayoutText:
            back_buffer = new visplay::layouts::BBWLayoutText(obj);
            break;
        case layouts::Identifiers::BBWLayoutVideo:
            back_buffer = new visplay::layouts::BBWLayoutVideo(obj);
            break;
        default:
            break;
    }

    if (back_buffer->valid) {
        send_text_reply(httpSocket, QHttpEngine::Socket::OK, "Backbuffer set to layout.\n");
    } 
    else {
        send_text_reply(httpSocket, QHttpEngine::Socket::OK, "Json object not valid.\n");
        back_buffer = nullptr;
    }
}

void Window::send_text_reply(QHttpEngine::Socket *http_socket, int status_code, const QByteArray& output_string)
{
    http_socket->setStatusCode(status_code);
    http_socket->setHeader("Content-Type", "text/plain");
    http_socket->writeHeaders();
    http_socket->write(output_string);
    http_socket->close();

}

void Window::long_poll_test(QHttpEngine::Socket *httpSocket)
{
    QEventLoop q;
    this->front_buffer->connect_event_loop(&q);
    q.exec();


    httpSocket->setStatusCode(QHttpEngine::Socket::OK);
    httpSocket->setHeader("Content-Type", "text/plain");
    httpSocket->writeHeaders();
    httpSocket->write("Testing.\n");
    httpSocket->close();
}

} // end namespace visplay
