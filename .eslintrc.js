module.exports = {
  env: {
    browser: true,
    es6: true,
    jquery: true,
    mocha: true,
    node: true,
  },

  plugins: [
    'babel',
    'mocha',
    'react',
  ],

  parser: 'babel-eslint',

  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    ecmaFeatures: {
      arrowFunctions: true,
      binaryLiterals: true,
      blockBindings: true,
      classes: true,
      defaultParams: true,
      destructuring: true,
      forOf: true,
      generators: true,
      modules: true,
      objectLiteralComputedProperties: true,
      objectLiteralDuplicateProperties: true,
      objectLiteralShorthandMethods: true,
      objectLiteralShorthandProperties: true,
      octalLiterals: true,
      regexUFlag: true,
      regexYFlag: true,
      spread: true,
      superInFunctions: true,
      templateStrings: true,
      unicodeCodePointEscapes: true,
      globalReturn: true,
      jsx: true,
    },
  },

  // Use the suggested ESLint settings as a base
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
  ],

  rules: {
    //
    // Possible Errors
    // These rules relate to possible syntax or logic errors in JavaScript code
    //
    'no-extra-parens': [1, 'all', { nestedBinaryExpressions: false }],
    'no-template-curly-in-string': 1,

    //
    // Best Practices
    //
    // These rules relate to better ways of doing things to help you avoid
    // problems.
    //
    'array-callback-return': 2,
    'block-scoped-var': 2,
    'class-methods-use-this': 0,
    'consistent-return': 2,
    curly: 2,
    'default-case': 1,
    'dot-location': [1, 'property'],
    'dot-notation': 1,
    eqeqeq: 2,
    'guard-for-in': 2,
    'no-alert': 2,
    'no-caller': 2,
    'no-div-regex': 2,
    'no-else-return': 1,
    'no-empty-function': 1,
    'no-eq-null': 2,
    'no-eval': 2,
    'no-extra-bind': 1,
    'no-floating-decimal': 1,
    'no-implicit-coercion': 2,
    'no-implicit-globals': 2,
    'no-implied-eval': 2,
    'no-invalid-this': 0,
    'no-iterator': 2,
    'no-labels': 2,
    'no-lone-blocks': 2,
    'no-loop-func': 2,
    'no-multi-spaces': [1, { ignoreEOLComments: false }],
    'no-multi-str': 1,
    'no-new': 1,
    'no-new-func': 2,
    'no-new-wrappers': 2,
    'no-octal-escape': 1,
    'no-proto': 2,
    'no-return-assign': [2, 'except-parens'],
    'no-return-await': 2,
    'no-script-url': 1,
    'no-self-compare': 1,
    'no-sequences': 2,
    'no-throw-literal': 2,
    'no-unmodified-loop-condition': 2,
    'no-unused-expressions': 1,
    'no-useless-call': 1,
    'no-useless-concat': 1,
    'no-useless-return': 1,
    'no-void': 2,
    'no-warning-comments': [1, {
      terms: ['todo', 'fixme', 'xxx', 'hack', 'debug'],
      location: 'start',
    }],
    'prefer-promise-reject-errors': 2,
    radix: 2,
    'require-await': 1,
    'vars-on-top': 1,
    'wrap-iife': [2, 'inside'],
    yoda: 1,

    //
    // Variables
    //
    // These rules have to do with variable declarations.
    //
    'no-label-var': 1,
    'no-shadow': 1,
    'no-undef-init': 2,
    'no-undefined': 2,
    'no-use-before-define': 2,

    //
    // Node.js and CommonJS
    //
    // These rules relate to code running in Node.js, or in browsers with
    // CommonJS.
    'callback-return': 1,
    'global-require': 1,
    'handle-callback-err': 1,
    'no-buffer-constructor': 2,
    'no-mixed-requires': 2,
    'no-path-concat': 1,
    'no-process-env': 1,

    //
    // Stylistic Issues
    //
    // These rules are purely matters of style and are quite subjective.
    //
    'array-bracket-spacing': [1, 'never'],
    'block-spacing': [1, 'always'],
    'brace-style': 1,
    camelcase: 0,
    'comma-dangle': [1, 'always-multiline'],
    'comma-spacing': [1, {
      before: false,
      after: true,
    }],
    'comma-style': [2, 'last'],
    'computed-property-spacing': 1,
    'eol-last': 1,
    'func-call-spacing': 1,
    indent: [1, 2, {
      SwitchCase: 1,
      VariableDeclarator: { var: 2, let: 2, const: 3 },
      FunctionDeclaration: { parameters: 'first', body: 1 },
      FunctionExpression: { parameters: 'first', body: 1 },
      CallExpression: { arguments: 1 },
      ArrayExpression: 1,
      ObjectExpression: 1,
    }],
    'jsx-quotes': [1, 'prefer-double'],
    'key-spacing': 1,
    'keyword-spacing': 1,
    'linebreak-style': 2,
    'max-len': [1, {
      code: 100,
      ignoreUrls: true,
      ignoreStrings: true,
      ignoreTemplateLiterals: true,
    }],
    'max-nested-callbacks': 1,
    'max-statements-per-line': 2,
    'new-cap': 1,
    'new-parens': 1,
    'no-array-constructor': 1,
    'no-bitwise': 1,
    'no-lonely-if': 1,
    'no-mixed-operators': 1,
    'no-multi-assign': 1,
    'no-multiple-empty-lines': 1,
    'no-nested-ternary': 1,
    'no-new-object': 1,
    'no-plusplus': 0,
    'no-tabs': 1,
    'no-trailing-spaces': 1,
    'no-unneeded-ternary': 1,
    'no-whitespace-before-property': 1,
    'object-curly-newline': [1, { consistent: true }],
    'object-curly-spacing': [1, 'always'],
    'one-var': [1, 'never'],
    'one-var-declaration-per-line': [1, 'always'],
    'operator-linebreak': [1, 'after'],
    'padded-blocks': [1, 'never'],
    'quote-props': [1, 'as-needed'],
    quotes: [1, 'single'],
    'require-jsdoc': [1, {
      require: {
        FunctionDeclaration: false,
        MethodDefinition: false,
        ClassDeclaration: true,
        ArrowFunctionExpression: false,
      },
    }],
    semi: [2, 'always'],
    'semi-style': [1, 'last'],
    'space-before-blocks': [1, 'always'],
    'space-before-function-paren': [1, {
      anonymous: 'never',
      named: 'never',
      asyncArrow: 'always',
    }],
    'space-in-parens': [1, 'never'],
    'space-infix-ops': 1,
    'space-unary-ops': [1, {
      words: true,
      nonwords: false,
    }],
    'spaced-comment': [1, 'always', {
      block: { balanced: true },
    }],
    'switch-colon-spacing': 1,
    'wrap-regex': 1,

    //
    // ECMAScript 6
    //
    // These rules relate to ES6, also known as ES2015.
    //
    'arrow-body-style': [1, 'as-needed', {
      requireReturnForObjectLiteral: true,
    }],
    'arrow-parens': [1, 'as-needed'],
    'arrow-spacing': 1,
    'generator-star-spacing': [1, {
      before: false,
      after: false,
    }],
    'no-duplicate-imports': 1,
    'no-useless-computed-key': 1,
    'no-useless-constructor': 1,
    'no-useless-rename': 1,
    'no-var': 1,
    'prefer-arrow-callback': 1,
    'prefer-const': 1,
    'prefer-numeric-literals': 1,
    'prefer-rest-params': 1,
    'prefer-spread': 1,
    'prefer-template': 1,
    'rest-spread-spacing': 1,
    'symbol-description': 1,
    'template-curly-spacing': 1,
    'yield-star-spacing': 1,

    //
    // eslint-plugin-mocha
    //
    'mocha/handle-done-callback': 2,
    'mocha/no-global-tests': 2,
    'mocha/no-identical-title': 2,
    'mocha/no-pending-tests': 1,
    'mocha/no-sibling-hooks': 2,
    'mocha/valid-suite-description': [1, '.'],
    'mocha/valid-test-description': [1, '^should\\s.+'],

    //
    // Babel
    //
    // https://medium.com/@chestozo/fixing-eslint-no-invalid-this-error-for-fat-arrow-class-methods-a56908ca8bb6
    'babel/no-invalid-this': 2,
  },

  globals: {},
};
