#    This file is part of Visplay.
#
#    Visplay is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Visplay is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Visplay.  If not, see <https://www.gnu.org/licenses/>.

import os
from pathlib import Path

import requests
import uri
import yaml
import aiohttp
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

from visplay.setup_sources import get_sources_list, sources_to_asset

# Every source needs to say whether the files it gets survive after an error,
# how to get an asset file, and how to get a playlist file.


def get_local_yaml(yaml_path):
    if yaml_path is None:
        return
    try:
        with open(yaml_path) as yaml_file:
            return yaml.load(yaml_file)

    except OSError:
        return {'error': 'An error parsing the yaml'}


class Source:
    """Root source class. Any source should override the __init__ function to
    do the loading of the source.
    """

    def __init__(self, name, uri: uri.URI, is_import=False):
        """Initialize a source.

        Arguments:
        """
        self.assets = {}
        self.sources = []
        self.layout = None
        self.is_import = is_import

    def __repr__(self):
        return f'<{type(self).__name__} assets={self.assets} sources={self.sources}>'

    async def new_content(self):
        """Returns true if the source has changed and false if it hasn't"""
        return False

    def close(self):
        """Clean any resources being used"""
        pass


class HTTPSource(Source):
    def __init__(self, name, uri: uri.URI, is_import=False):
        super().__init__(name, uri, is_import=is_import)
        self.uri = uri
        self.hashUri = (self.uri / self.uri.path.parent / f'{self.uri.path.stem}.meta').uri
        try:
            if self.is_import:
                with requests.get(uri.base, verify=False) as remote_file:
                    self.sources = get_sources_list(remote_file.content)
                    self.assets = sources_to_asset(name, self.sources)
                    self.layout = None
            else:
                with requests.get(uri.base, verify=False) as remote_file:
                    self.assets = yaml.load(remote_file.content)

        except ConnectionError:
            return {'error': 'URL not available'}
        self.hash = requests.get(self.hashUri, verify=False).text

    async def play(self, session, url, asset):
        await session.post(
            (url / 'backbuffer/change_layout').uri,
            json={'layout': 4, 'header': '', 'path': asset})
        await session.post((url / 'swap_buffers').uri)
        await session.get((url / '/long_poll_test').uri)

    async def new_content(self):
        session = aiohttp.ClientSession(connector=aiohttp.TCPConnector(verify_ssl=False))
        h = await session.get(self.hashUri)
        h = await h.text()
        await session.close()
        if h != self.hash:
            self.hash = h
            return True
        return False

    def close(self):
        pass


class IPFSSource(Source):
    """Allow users to specify an IPFS hash as a source.

    If the source
    """

    def __init__(self, name, uri: uri.URI, is_import=False):
        super().__init__(name, uri, is_import=is_import)


class PathSource(Source):
    """Allow users to specify a path as a source.

    If the path is a directory, it will load all ``*.sources.yaml`` files as
    sources, and all other ``.yaml`` files as assets.  All non-YAML files will
    be loaded as assets asset with the filename as the name of the asset.

    If the path is a file, it will be added as an asset with the filename as
    the name of the asset.
    """

    def __init__(self, name, uri: uri.URI, is_import=False):
        super().__init__(name, uri, is_import=is_import)
        path = Path(uri.path)
        self.observer = Observer()
        self.updated = False
        event_handler = PatternMatchingEventHandler(patterns=[os.fspath(uri.path)])
        event_handler.on_any_event = self.update_file
        self.observer.schedule(event_handler, os.path.split(uri.path)[0], recursive=True)
        self.observer.start()

        if not os.path.exists(path):
            raise Exception(f'{path} does not exist.')

        for file in (os.listdir(path) if os.path.isdir(path) else [path]):
            file_path = path.joinpath(file)

            if file_path.suffix == '.yaml':
                if self.is_import and '.sources' in file_path.suffixes:
                    with open(file_path) as source_file:
                        # Recursively discover all sources
                        self.sources += get_sources_list(source_file)

                        # Namespace the assets
                        self.assets.update(
                            sources_to_asset(name, self.sources))
                        self.layout = None
                else:
                    self.assets.update(get_local_yaml(file_path))
            else:
                self.assets[file_path.name] = str(file_path)

    def update_file(self, a):
        self.updated = True

    async def new_content(self):
        return self.updated

    def close(self):
        self.observer.stop()


# A list of sources following a basic interface.
source_constructors = {
    'file': PathSource,
    'http': HTTPSource,
    'https': HTTPSource,
    'ipfs': IPFSSource,
    'path': PathSource,
}
