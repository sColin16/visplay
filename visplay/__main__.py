#    This file is part of Visplay.
#
#    Visplay is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Visplay is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Visplay.  If not, see <https://www.gnu.org/licenses/>.

"""Main entrypoint for Visplay."""
from argparse import ArgumentParser, Namespace
import aiohttp
import asyncio
from sys import stderr

import prompt

from visplay.config import Config
from visplay.setup_sources import get_sources_list
from visplay.setup_sinks import run_sinks

from visplay.routes import *

from visplay.routes.server_thread import ServerThread

import bottle

def playable_generator(sources):
    """Return a generator that will infinitely return new things to play."""
    running = True
    while running:
        playlist = ['main']
        assets = {}

        # Find the assets in each source and combine them
        for source in sources:
            if source.assets:
                assets.update(source.assets)

        # Build and play the stack
        while playlist:
            current = playlist.pop()
            if current in assets:
                if type(assets[current]) is list:
                    for asset in reversed(assets[current]):
                        playlist.append(asset)
                elif assets[current] in assets:
                    playlist.append(assets[assets[current]])
                else:
                    yield assets[current]
            else:
                print("ERROR finding,", current)


def main():
    """The main entrypoint for program when run standalone."""

    # CLI Arguments
    parser = ArgumentParser(description='Visplay Server')
    parser.add_argument(
        '-c',
        '--config',
        dest='config',
        type=open,
        default=Config.default_config,
        help='Specify a custom configuration file to load.',
    )
    parser.add_argument('port', nargs='?', default=8080, type=int)
    parser.add_argument('-d', '--debug', action='store_true')

    try:
        # Get the arguments
        args = parser.parse_args()
    except FileNotFoundError as e:
        # Only ask to create a config file if it's in the default location. This
        # is because multiple files need to be created. Otherwise, exit the
        # program with an error
        if e.filename == Config.default_config:
            response = prompt.regex('^([yYnN]).*$',
                                    empty=True,
                                    prompt='\n'.join([
                                        'Would you like to create the config file',
                                        f'{e.filename} with default settings? [y/N] '
                                    ]))

            if response and response.group(1) in ('y', 'Y'):
                Config.create_default_config()
                args = Namespace()
                args.config = open(Config.default_config)
            else:
                exit(0)
        else:
            print(f'The configuration file {e.filename} does not exist.', file=stderr)
            exit(1)

    server_thread = ServerThread(daemon=True)
    server_thread.run(port=args.port, debug=args.debug)

    while True:
        sources, generator = loadConf(args.config)
        for asset in generator:
            if asyncio.run(race_videos(asset, sources)):
                for source in sources:
                    source.close()
                break


def loadConf(config: int):
    """Loads the config file and everything it references"""
    try:
        Config.load_from_yaml(config)
    except Exception as e:
        print(f'There was an error parsing the configuration file:\n\t{e}\n',
              file=stderr)
        exit(0)

    if not Config.get('sources'):
        raise KeyError('No sources found in configuration file.')

    with open(Config.sources) as source_file:
        sources = get_sources_list(source_file)
        generator = playable_generator(sources)
    return (sources, generator)


async def race_videos(asset, sources):
    """Starts a race between the current video and the configuration watchers.
    If the configuration code wins, the function returns true"""
    session = aiohttp.ClientSession()
    # Sinks accept instructions on what to play and return back the video progress
    # Watches for the video to finish
    video_watch = asyncio.create_task(run_sinks(session, Config.sinks, asset))
    # Watches for a source file to change
    source_watch = asyncio.create_task(check_sources(sources))
    done, _ = await asyncio.wait({video_watch, source_watch}, return_when=asyncio.FIRST_COMPLETED)
    await session.close()
    # If source_watch finished then return true if it won the race
    return source_watch in done


async def check_sources(sources):
    """Checks if any of the sources have been updated. Stays in the loop until
    one of the sources changes"""
    should_exit = False
    while not should_exit:
        should_exit = Config.watch()
        for source in sources:
            should_exit |= await source.new_content()
        await asyncio.sleep(3)


if __name__ == '__main__':
    main()
