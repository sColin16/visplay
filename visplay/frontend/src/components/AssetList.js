import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';

import TableWithAddButton from './core/TableWithAddButton';

import PropTypes from 'prop-types';

const Asset = ({ asset, on_delete_click, on_edit_click }) =>
  <TableRow>
    <TableCell>
      <Tooltip title="Edit">
        <IconButton aria-label="Edit">
          <EditIcon onClick={() => on_edit_click(asset.name)} />
        </IconButton>
      </Tooltip>
      <Tooltip title="Delete">
        <IconButton aria-label="Delete">
          <DeleteIcon onClick={() => on_delete_click(asset.name)} />
        </IconButton>
      </Tooltip>
    </TableCell>
    <TableCell>{asset.name}</TableCell>
    <TableCell>{asset.uri}</TableCell>
  </TableRow>;
Asset.propTypes = {
  asset: PropTypes.object.isRequired,
  on_delete_click: PropTypes.func.isRequired,
  on_edit_click: PropTypes.func.isRequired,
};

/**
 * A list of all the assets.
 *
 * @class
 */
class AssetList extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    assets: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    on_addButton_click: PropTypes.func.isRequired,
    on_asset_delete: PropTypes.func.isRequired,
    on_asset_edit: PropTypes.func.isRequired,
  };

  render() {
    const {
      classes, assets, loading, on_addButton_click, on_asset_delete, on_asset_edit,
    } = this.props;

    return (
      <TableWithAddButton className={classes.card} headerText="Assets" loading={loading}
        on_addButton_click={on_addButton_click.bind(this)}>
        <TableHead>
          <TableRow>
            <TableCell>Edit/Delete</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>URI</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            assets.map((a, i) =>
              <Asset key={i} asset={a}
                on_delete_click={on_asset_delete}
                on_edit_click={on_asset_edit}
              />)
          }
        </TableBody>
      </TableWithAddButton>
    );
  }
}

export default withStyles(() => {
  return {};
})(AssetList);
