import React from 'react';

import '../styles/App.css';
import AppTabs from './AppTabs.js';
import MainToolbar from './MainToolbar';

/**
 * Main application class
 *
 * @class
 */
const App = () =>
  <div>
    <MainToolbar />
    <AppTabs />
  </div>;

export default App;
