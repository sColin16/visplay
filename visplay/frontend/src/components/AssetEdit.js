import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import PropTypes from 'prop-types';

/**
 * A list of all the assets.
 *
 * @class
 */
class AssetEdit extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    is_editing: PropTypes.bool.isRequired,
    initial_edit_state: PropTypes.object,
    open: PropTypes.bool.isRequired,
    on_close: PropTypes.func.isRequired,
    on_add_edit: PropTypes.func.isRequired,
  };

  fresh_state = () => {
    return { name: '', uri: '', nameError: '', uriError: '' };
  }

  state = this.fresh_state()

  on_field_change = name => event => {
    this.setState({
      [name] : event.target.value,
      [name + "Error"]: "",
    });
  }

  on_blur = name => () => {
    if(this.state[name] === '') {
      this.setState({
        [name + "Error"]: (name === "name" ? "Name" : "URI") + " required",
      });
    }
  }

  on_form_submit = () => {
    if (this.state.name === '' || this.state.uri === '') {
      this.setState({
        nameError: this.state.name === '' ? 'Name required' : '',
        uriError: this.state.uri === '' ? 'URI required' : '',
      });

      return;
    }

    // TODO: Serve-side validation
    this.props.on_add_edit(this.state);
    this.setState(this.fresh_state());
  }

  render() {
    const { classes, open, on_close, is_editing, initial_edit_state } = this.props;

    const clean_close = (...rest) => {
      this.setState(this.fresh_state());
      on_close.call(on_close, ...rest);
    };

    const on_addEditButton_click = () => {
      this.on_form_submit();
    };

    const onEntering = () => {
      if (is_editing) {
        this.setState(initial_edit_state);
      }
    };

    const on_keyPress = event => {
      if (event.key === 'Enter') {
        this.on_form_submit();
      }
    };

    return (
      <Dialog
        open={open}
        onClose={clean_close}
        onEntering={onEntering}
        onKeyPress={on_keyPress}
        maxWidth="md"
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">{is_editing ? 'Edit' : 'Add'} Asset</DialogTitle>
        <DialogContent>
          <TextField
            error={Boolean(this.state.nameError)}
            required
            id="name"
            label="Asset Name"
            value={this.state.name}
            onChange={this.on_field_change('name')}
            onBlur={this.on_blur('name')}
            className={classNames(classes.textField, classes.nameField)}
            margin="normal"
            helperText={this.state.nameError}
          />
          <TextField
            error={Boolean(this.state.uriError)}
            required
            id="uri"
            label="Asset URI"
            value={this.state.uri}
            onChange={this.on_field_change('uri')}
            onBlur={this.on_blur('uri')}
            className={classNames(classes.textField, classes.uriField)}
            margin="normal"
            helperText={this.state.uriError}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={clean_close} color="primary">
            Cancel
          </Button>
          <Button onClick={on_addEditButton_click} color="primary">
            {is_editing ? 'Edit' : 'Add'}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(theme => {
  return {
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
    },
    nameField: { width: 200 },
    uriField: { width: 400 },
  };
})(AssetEdit);
