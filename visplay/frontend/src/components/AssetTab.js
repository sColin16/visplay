import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';

import AssetList from './AssetList';
import AssetEdit from './AssetEdit';


/**
 * Main tab container
 */
class AssetTab extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    assets: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    on_assets_change: PropTypes.func.isRequired,
  }

  state = {
    editorOpen: false,
    is_editing: false,
    editing_name: null,
    initial_edit_state: null,
  }

  on_addButton_click() {
    this.setState({ editorOpen: true });
  }

  on_AssetEdit_close() {
    this.setState({ editorOpen: false, is_editing: false });
  }

  on_AssetEdit_add_edit(new_asset) {
    if (this.state.is_editing) {
      this.props.on_assets_change('edit', {
        asset_name: this.state.editing_name,
        new_value: new_asset,
      });
    } else { // Add
      this.props.on_assets_change('add', new_asset);
    }
    this.setState({ editorOpen: false, is_editing: false });
  }

  on_AssetList_delete(asset_name) {
    this.props.on_assets_change('delete', { asset_name: asset_name });
  }

  on_AssetList_edit(asset_name) {
    this.setState({
      editorOpen: true,
      is_editing: true,
      editing_name: asset_name,
      initial_edit_state: this.props.assets.filter(a => a.name === asset_name)[0],
    });
  }

  render() {
    const { assets, loading } = this.props;
    const { editorOpen, is_editing, initial_edit_state } = this.state;

    return (
      <div>
        <AssetList assets={assets} loading={loading}
          on_asset_delete={this.on_AssetList_delete.bind(this)}
          on_asset_edit={this.on_AssetList_edit.bind(this)}
          on_addButton_click={this.on_addButton_click.bind(this)} />
        <AssetEdit open={editorOpen}
          is_editing={is_editing}
          initial_edit_state={initial_edit_state}
          on_add_edit={this.on_AssetEdit_add_edit.bind(this)}
          on_close={this.on_AssetEdit_close.bind(this)} />
      </div>
    );
  }
}

export default withStyles(() => {
  return {};
})(AssetTab);
