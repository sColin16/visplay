from bottle import hook, response, route, static_file


@route('/')
def index():
    print('Now trying to get the frontend code')
    return dist('index.html')


@route('/<filename:path>')
def dist(filename):
    return static_file(filename, root='dist')


@hook('after_request')
def enable_cors():
    add_cors_headers()


@route('/<:re:.*>', method='OPTIONS')
def enable_cors_for_all_routes():
    add_cors_headers()


def add_cors_headers():
    response.set_header('Access-Control-Allow-Origin', '*')
    response.add_header('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS')
    response.add_header('Access-Control-Allow-Headers',
                        'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
