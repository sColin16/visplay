import json
import os
import datetime
import yaml

from bottle import post, request, response, route, template

from visplay.config import Config

assets_file = os.path.join(Config._config_folder, 'assets.yaml')
assets_file_meta = os.path.join(Config._config_folder, 'assets.meta')


def get_assets():
    with open(assets_file) as af:
        return yaml.load(af)


def write_assets(new_value):
    with open(assets_file, 'w+') as af:
        yaml.dump(new_value, af, default_flow_style=False)

    with open(assets_file_meta, 'w+') as afm:
        afm.write(str(datetime.datetime.now()))

@route('/assets')
def assets():
    response.content_type = 'application/json'
    assets = [{'name': name, 'uri': uri} for name, uri in get_assets().items()]
    return json.dumps(assets)


@post('/asset/add')
def new_asset():
    response.set_header('Access-Control-Allow-Origin', '*')

    assets = get_assets()
    assets[request.json['name']] = request.json['uri']
    write_assets(assets)


@post('/asset/delete')
def delete_asset():
    asset_name = request.json['asset_name']
    assets = get_assets()
    assets.pop(asset_name, None)
    write_assets(assets)


@post('/asset/edit')
def edit_asset():
    json = request.json
    asset_name = json['asset_name']
    new_asset_name = json['new_value']['name']
    new_asset_uri = json['new_value']['uri']

    assets = get_assets()

    if asset_name != new_asset_name:
        assets.pop(asset_name, None)
        assets[new_asset_name] = new_asset_uri
    else:
        assets[asset_name] = new_asset_uri
    write_assets(assets)
