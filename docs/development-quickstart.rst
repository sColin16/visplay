Development Quickstart Guide
============================

This guide is intended to get you started on developing Visplay as quickly as
possible.

Dependencies
------------

Make sure that you have the following installed and available on your system's
``PATH``.

The following dependencies are required for all work on any part of Visplay.

- Python 3.7 or greater (probably something like
  ``python3`` in your package manager)
- Pip 3 (probably something like ``python3-pip`` in your package manager)
- ``youtube-dl`` (depends on your package manager)

If you plan to work on **Visplay Server** or **Visplay Renderer**, these
additional dependencies are required.

- ``libmpv`` and ``mpv`` (depends on your package manager)
- ``qt5-qtwebengine-devel qt5-qtwebengine mingw64-qt5-qmake mingw32-qt5-qmake`` (depends on your package manager)
- ``qhttpengine qhttpengine-devel``

.. note::

  Fedora users must install the RPM Fusion Repos for MPV*

If you plan to work on the **Configuration Generator**, these additional
dependencies are required:

- ``nodejs`` and ``npm`` (to compile the frontend)

Installation
------------

1. Clone the repository and install the Python dependencies::

        $ git clone https://gitlab.com/ColoradoSchoolOfMines/visplay.git
        $ cd visplay
        $ pip3 install --user -r requirements.txt

2. Install the appropriate additional requirements

   - **Visplay Server and Visplay Renderer**

     1. Install the dependencies::

            $ pip3 install -e . --user

        .. note::

            If you have already installed, add ``--update`` to the ``pip install``
            command.

     2. Run ``visplay``. It will prompt you to create the
             `Default Configuration`_.

   - **Visplay Configuration Generator**

     Let NPM handle the installation::

        $ cd config-generator
        $ npm install

Running Visplay
---------------

- **Visplay Server and Visplay Renderer**

  Run the ``visplay`` command.

- **Visplay Configuration Generator**

  Run ``npm start`` from the ``config-generator`` directory.

- **Visplay Server Docker**

  ::

    docker login registry.gitlab.com
    docker run -p 8080:8080 registry.gitlab.com/coloradoschoolofmines/visplay/server:stable

Configuration
-------------

By default, Visplay Server and Visplay Configuration Generator look in
``$XDG_CONFIG_HOME``, and ``$HOME/.config`` for ``visplay/visplay.yaml``.  This
is the main local configuration and points to any number of ``assets.yaml``
files and ``playlists.yaml`` files (whether local or remote).

You can use the ``-c`` or ``--config`` parameters to choose what configuration
file to use.

Default Configuration
^^^^^^^^^^^^^^^^^^^^^

To initialize the default configuration, run ``visplay`` without existing
configuration files. It will ask you if you want to generate the default ones.

If you want to override your existing configuration, delete your existing
configuration files and run ``visplay``.
